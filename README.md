-------REQUISITOS---------
1. java JDK 8 instalado

-------INSTALAÇÃO----------

1. Mova a pasta AriSync para /opt
2. Mova o arquivo AriSync.desktop para /usr/share/applications
3. Procure o executavel Ari Sync no menu de aplicativos e execute ele ou, execute o arquivo /opt/AriSync/AriSync em um terminal com o comando "bash /opt/AriSync/AriSync"

-------teste de serviço----

Após gerar o "Link de conexão" e iniciar a sincronização, você pode testar o serviço para ver se tudo está ocorrendo bem. Basta seguir as etapas abaixo:

1. Abre seu navegador e digite: localhost:8080/h2-console/
2. Caso não apareça uma tela de login, tente iniciar a sincronização pelo Ari Sync Desktop novamente.
3. Caso apareça uma telo para login, seu serviço está executando corretamente, mas caso deseje fazer o acesso, siga os passos abaixo.
3. Na tela de login, no campo "JDBC URL", digite: jdbc:h2:file:.arisync
4. No campo "User Name", digite: ari
5. No campo "Password", digite: unicodetec

A sincronização pode levar alguns minutos para ser feita devida a verificação de quais arquivos devem ser sincronizados, esse tempo pode ser maior ou menor dependendo da velocidade da sua internet e também, da velocidade em que seu equipamentos conseguem trabalhar.
