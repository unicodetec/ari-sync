/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arisync;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 *
 * @author bruno
 */
public class MainController implements Initializable {
    
    ResultSet rs = null;
    Runtime run = Runtime.getRuntime();
    Process p = null;
    
    @FXML private Button btn_tutorial;
    @FXML private Button btn_salvar;
    @FXML private Button btn_fechar;
    @FXML private TextField txt_link;
    @FXML private TextField txt_mask;
    @FXML private TextField txt_gw;
    
    String ip,mask,gw;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
    
    @FXML
    public void buscaDados(ActionEvent event) throws IOException{
        String cmd = "cinnamon-settings network";
        run.exec(cmd);
    }
    
    @FXML
    void criarPasta(ActionEvent event){
        try {
        Runtime run = Runtime.getRuntime();
        String comando;
        comando = "bash /opt/AriSync/scripts/criarPasta/criaPasta";
        run.exec(comando);
        
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @FXML
    void permissao(ActionEvent event){
        try {
        Runtime run = Runtime.getRuntime();
        String comando;
        comando = "bash /opt/AriSync/scripts/criarPasta/permissao";
        run.exec(comando);
        
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @FXML
    void link(ActionEvent event){
        try {
        Runtime run = Runtime.getRuntime();
        String comando;
        comando = "bash /opt/AriSync/scripts/criarPasta/Link";
        run.exec(comando);
        
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @FXML
    void iniciarServico(ActionEvent event){
        try {
        Runtime run = Runtime.getRuntime();
        String comando;
        comando = "bash /opt/AriSync/scripts/servico";
        run.exec(comando);
        
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @FXML
    void tutorial(){
        Parent tela = null;
        try {
            tela = FXMLLoader.load(getClass().getResource("tela/Tutorial.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
                Stage stage = new Stage();
                Scene scene = new Scene(tela);
                
                stage.setScene(scene);
                stage.show();
    }
    
    @FXML
    void gerarLink(){
        String ip = null;
        InetAddress ia = null;
        Enumeration nis = null;
        try {
            nis = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e) {
            e.printStackTrace();
        }

        while (nis.hasMoreElements()) {
            NetworkInterface ni = (NetworkInterface) nis.nextElement();
            Enumeration ias = ni.getInetAddresses();

            while (ias.hasMoreElements()) {
                ia = (InetAddress) ias.nextElement();
                
                if (ia.getHostAddress().contains("192")) {
                ip=ia.getHostAddress();    
                }
                if (!ni.getName().equals("lo")) {
                }
            }
        }
        txt_link.setText("http://"+ip+":8080");
    }
    
    @FXML
    void semIp(){
        Parent tela = null;
        try {
            tela = FXMLLoader.load(getClass().getResource("tela/SemIP.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
                Stage stage = new Stage();
                Scene scene = new Scene(tela);
                
                stage.setScene(scene);
                stage.show();
    }
    
    @FXML
    void fechar(ActionEvent event) {
        try {
            Stage stage = new Stage();
            
            stage = (Stage) btn_fechar.getScene().getWindow();
            stage.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
        
    }
    
}