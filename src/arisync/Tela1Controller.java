package arisync;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/*
 * FXML Controller class
 *
 * @author bruno
 */

public class Tela1Controller implements Initializable {
    
    Runtime run = Runtime.getRuntime();
    
    @FXML private Button btn_avancar;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    @FXML
    public void buscaDados(ActionEvent event) throws IOException{
        String cmd = "cinnamon-settings network";
        run.exec(cmd);
    }
    
    @FXML
    void avancar(){
        Parent tela = null;
        try {
            tela = FXMLLoader.load(getClass().getResource("tela/Tela2.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Tela1Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
                Stage stage = new Stage();
                Scene scene = new Scene(tela);
                
                stage.setScene(scene);
                stage.show();
                
                stage = (Stage) btn_avancar.getScene().getWindow();
                stage.close();
    }
    
}