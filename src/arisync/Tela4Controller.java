/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arisync;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author bruno
 */
public class Tela4Controller implements Initializable {

    Runtime run = Runtime.getRuntime();
    Process p = null;
    
    @FXML private Button btn_avancar;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    public void consultaIp(ActionEvent event) throws IOException{
        run = Runtime.getRuntime();
        p = run.exec(new String[]{"/bin/sh", "-c", "bash /home/bruno/NetBeansProjects/AriSync/consultaIp.bash"});
        Scanner scanner = new Scanner(p.getInputStream());
        String resultado = scanner.next();
    }
    
    @FXML
    void avancar(){
        Parent tela = null;
        try {
            tela = FXMLLoader.load(getClass().getResource("tela/Tela5.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(Tela4Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
                Stage stage = new Stage();
                Scene scene = new Scene(tela);
                
                stage.setScene(scene);
                stage.show();
                
                stage = (Stage) btn_avancar.getScene().getWindow();
                stage.close();
    }
    
}
